

def get_data():
    foods = []
    potential_allergens = {}
    with open('day21.data') as file:
        for line in file:
            ingredients, allergens = line.split(' (contains ')
            allergens = allergens.replace(')\n', '')
            ingredients = ingredients.split(' ')
            allergens = allergens.split(', ')
            foods.append(ingredients)
            for a in allergens:
                if a not in potential_allergens:
                    potential_allergens[a] = set(ingredients)
                else:
                    potential_allergens[a] &= set(ingredients)
    return foods, potential_allergens


def cross_check(potential_allergens):
    mutated = True
    while mutated:
        mutated = False
        for allergen, ingredients in potential_allergens.items():
            if len(ingredients) == 1:
                ingredient = next(iter(ingredients))
                for allergen2, ingredients2 in potential_allergens.items():
                    if allergen != allergen2:
                        if ingredient in ingredients2:
                            mutated = True
                            ingredients2 -= ingredients


def main():
    foods, potential_allergens = get_data()
    cross_check(potential_allergens)

    allergens = set()
    for tmp in potential_allergens.values():
        allergens |= tmp
    count = 0
    for food in foods:
        count += sum(1 for ingredient in food if ingredient not in allergens)
    print(count)

    sorted_allergens = []
    for tmp in sorted(potential_allergens):
        sorted_allergens.append(next(iter(potential_allergens[tmp])))
    print(','.join(sorted_allergens))


if __name__ == '__main__':
    main()
