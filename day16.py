

def load_ticket_data():
    ranges = {}
    tickets = []

    with open('day16.data') as file:
        line = file.readline()
        while len(line) > 1:
            area, values = line.split(': ')
            ranges[area] = []
            for val in values.split(' or '):
                ranges[area].append(tuple(int(x) for x in val.split('-')))
            line = file.readline()

        file.readline()
        my_ticket = [int(x) for x in file.readline().split(',')]
        file.readline()
        file.readline()

        line = file.readline()
        while len(line) > 1:
            tickets.append([int(x) for x in line.split(',')])
            line = file.readline()
    return ranges, my_ticket, tickets


def check_invalid_tickets(ranges, tickets):
    valid_tickets = []
    invalids = []
    for ticket in tickets:
        valid_ticket = True
        for val in ticket:
            valid = False
            for r1, r2 in ranges.values():
                if (r1[0] <= val <= r1[1]) or (r2[0] <= val <= r2[1]):
                    valid = True
            if not valid:
                valid_ticket = False
                invalids.append(val)
        if valid_ticket:
            valid_tickets.append(ticket)
    return valid_tickets, invalids


def get_possible_classes(ranges, tickets):
    classes = [[] for _ in range(len(tickets[0]))]

    for class_name, r in ranges.items():
        r1, r2 = r
        for idx in range(len(classes)):
            valid = True
            for ticket in tickets:
                if not ((r1[0] <= ticket[idx] <= r1[1]) or (r2[0] <= ticket[idx] <= r2[1])):
                    valid = False
            if valid:
                classes[idx].append(class_name)
    return classes


def main():
    ranges, my_ticket, tickets = load_ticket_data()

    valid_tickets, invalids = check_invalid_tickets(ranges, tickets)
    print(sum(invalids))

    valid_tickets.append(my_ticket)
    possible_classes = get_possible_classes(ranges, valid_tickets)

    mutated = True
    while mutated:
        mutated = False
        for idx, names in enumerate(possible_classes):
            if len(names) == 1:
                for idx2, names2 in enumerate(possible_classes):
                    if idx != idx2 and names[0] in names2:
                        del names2[names2.index(names[0])]
                        mutated = True
    prod = 1
    for idx, names in enumerate(possible_classes):
        if 'departure' in names[0]:
            prod *= my_ticket[idx]
    print(prod)


if __name__ == '__main__':
    main()
