

def add_rule(rules, line):
    parts = line.split(' ')
    key = parts[0][:-1]
    parts = parts[1:]
    matchings = []
    while '|' in parts:
        idx = parts.index('|')
        matchings.append(parts[:idx])
        parts = parts[idx + 1:]
    matchings.append(parts)
    rules[key] = matchings


def load_data():
    rules = {}
    msgs = []
    in_rule_section = True
    with open('day19.data') as file:
        for line in file:
            line = line.replace('\n', '').replace('"', '')
            if len(line) == 0:
                in_rule_section = False
            elif in_rule_section:
                add_rule(rules, line)
            else:
                msgs.append(line)
    return rules, sorted(msgs)


def get_matching_msgs(msgs, idx, pattern):
    start, stop = None, None
    for msg_idx, msg in enumerate(msgs):
        if start is None:
            if msg[idx:idx + len(pattern)] == pattern:
                start, stop = msg_idx, msg_idx
        elif msg[idx:idx + len(pattern)] == pattern:
            stop = msg_idx
        else:
            break
    if start is None:
        return None
    return msgs[start:stop + 1]


def generate_sub_pattern(msgs, rules, sub_rule, max_len, idx):
    if idx < max_len:
        p = sub_rule[0]
        if len(sub_rule) == 1:
            if p in rules:
                for x in generate_pattern(msgs, rules, rules[p], max_len, idx):
                    yield x
            else:
                yield p
        else:
            if p in rules:
                for x in generate_sub_pattern(msgs, rules, sub_rule[:1], max_len, idx):
                    inner_msgs = get_matching_msgs(msgs, idx, x)
                    if inner_msgs is not None:
                        for y in generate_sub_pattern(inner_msgs, rules, sub_rule[1:], max_len, idx + len(x)):
                            yield x + y
            else:
                inner_msgs = get_matching_msgs(msgs, idx, p)
                if inner_msgs is not None:
                    for y in generate_sub_pattern(inner_msgs, rules, sub_rule[1:], max_len, idx + len(p)):
                        yield p + y


def generate_pattern(msgs, rules, rule, max_len, idx=0):
    if idx < max_len:
        for part in rule:
            for x in generate_sub_pattern(msgs, rules, part, max_len, idx):
                yield x


def solve(msgs, rules):
    matches = 0
    max_len = max(len(msg) for msg in msgs)
    for pattern in generate_pattern(msgs, rules, rules['0'], max_len):
        if pattern in msgs:
            matches += 1
            del msgs[msgs.index(pattern)]
    print(matches)


def main():
    rules, msgs = load_data()

    solve(msgs.copy(), rules)

    add_rule(rules, '8: 42 | 42 8')
    add_rule(rules, '11: 42 31 | 42 11 31')
    solve(msgs, rules)


if __name__ == '__main__':
    main()
