

valid = 0
valid2 = 0
with open('day02.data') as file:
    for line in file:
        tmp, letter, password = line.split(' ')
        letter_min, letter_max = (int(x) for x in tmp.split('-'))
        letter = letter[0]

        if letter_min <= password.count(letter) <= letter_max:
            valid += 1
        if (password[letter_min-1] == letter) ^ (password[letter_max-1] == letter):
            valid2 += 1

print(valid, valid2)