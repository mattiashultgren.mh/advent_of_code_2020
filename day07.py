
import networkx as nx


def get_data_graph():
    bags = nx.DiGraph()
    with open('day07.data') as file:
        for line in file:
            words = line.replace('\n', '').split(' ')
            bag = ' '.join(words[:2])
            if words[4] != 'no':
                for idx in range(4, len(words), 4):
                    bags.add_edge(bag, ' '.join(words[idx+1:idx+3]), weight=int(words[idx]))
    return bags


def find_number_of_bags_inside(bags, bag):
    bags_inside = 0
    for bag_inside in bags.successors(bag):
        count = bags[bag][bag_inside]['weight']
        bags_inside += count * (1 + find_number_of_bags_inside(bags, bag_inside))
    return bags_inside


def main():
    bags = get_data_graph()
    print(len(nx.algorithms.dag.ancestors(bags, 'shiny gold')))
    print(find_number_of_bags_inside(bags, 'shiny gold'))


if __name__ == '__main__':
    main()
