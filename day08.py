
import collections


Instruction = collections.namedtuple('Instruction', 'opcode arg')


class Computer:
    def __init__(self):
        self.program = []
        self.acc = 0
        self.pc = 0

    def reset(self):
        self.acc = 0
        self.pc = 0

    def load_from_file(self, filename):
        with open(filename) as file:
            self.program = []
            for line in file:
                opcode, arg = line.replace('\n', '').split(' ')
                instr = Instruction(opcode, int(arg))
                self.program.append(instr)

    def execute(self, instr):
        pc, acc = {
            'nop': lambda arg: (1, 0),
            'jmp': lambda arg: (arg, 0),
            'acc': lambda arg: (1, arg)
        }[instr.opcode](instr.arg)
        self.pc += pc
        self.acc += acc

    def run(self):
        executed = [False] * len(self.program)

        self.reset()
        while not executed[self.pc]:
            executed[self.pc] = True
            self.execute(self.program[self.pc])
            if self.pc == len(self.program):
                return True
        return False


def main():
    comp = Computer()
    comp.load_from_file('day08.data')
    comp.run()
    print(comp.acc)

    for idx in range(len(comp.program)):
        instr = comp.program[idx]
        if instr.opcode == 'acc':
            pass
        else:
            if instr.opcode == 'nop':
                comp.program[idx] = Instruction('jmp', instr.arg)
            else:
                comp.program[idx] = Instruction('nop', instr.arg)
            if comp.run():
                print(comp.acc)
                break
            comp.program[idx] = instr


if __name__ == '__main__':
    main()
