

def find_tile_location(direction):
    x, y = 0, 0
    while len(direction) > 0:
        step = direction[:2] if direction[0] in ['s', 'n'] else direction[0]
        direction = direction[len(step):]
        x, y = {'e': lambda xx, yy: (xx + 2, yy),
                'w': lambda xx, yy: (xx - 2, yy),
                'sw': lambda xx, yy: (xx - 1, yy + 1),
                'se': lambda xx, yy: (xx + 1, yy + 1),
                'nw': lambda xx, yy: (xx - 1, yy - 1),
                'ne': lambda xx, yy: (xx + 1, yy - 1),
        }[step](x, y)
    return x, y


def get_neighbor(x, y):
    yield x + 2, y
    yield x - 2, y
    yield x - 1, y + 1
    yield x + 1, y + 1
    yield x - 1, y - 1
    yield x + 1, y - 1


def mutate(black_tiles):
    black_tiles2 = set()
    white_tiles_to_check = set()
    for x, y in black_tiles:
        for tile in get_neighbor(x, y):
            white_tiles_to_check.add(tile)
        next_to = sum(1 for tile in get_neighbor(x, y) if tile in black_tiles)
        if next_to == 1:
            black_tiles2.add((x, y))
    for x, y in white_tiles_to_check:
        next_to = sum(1 for tile in get_neighbor(x, y) if tile in black_tiles)
        if next_to == 2:
            black_tiles2.add((x, y))
    return black_tiles2


def main():
    black_tiles = set()
    with open('day24.data') as file:
        for line in file:
            tile_location = find_tile_location(line.replace('\n', ''))
            if tile_location in black_tiles:
                black_tiles.remove(tile_location)
            else:
                black_tiles.add(tile_location)
    print(len(black_tiles))

    for _ in range(100):
        black_tiles = mutate(black_tiles)
    print(len(black_tiles))

if __name__ == '__main__':
    main()
