
import collections


BusData = collections.namedtuple('BusData', 'idx id waiting_time magic_number')


def main():
    bus_data = []
    with open('day13.data') as file:
        now = int(file.readline())
        buses = file.readline().split(',')
        for idx, bus in enumerate(buses):
            if bus != 'x':
                bus_id = int(bus)
                waiting_time = (bus_id - now % bus_id) % bus_id
                bus_data.append(BusData(idx, bus_id, waiting_time, bus_id * waiting_time))
    print(sorted(bus_data, key=lambda x: x.waiting_time)[0].magic_number)

    n, period = 0, bus_data[0].id
    for bus in bus_data[1:]:
        while True:
            if (n + bus.idx) % bus.id == 0:
                break
            n += period
        period *= bus.id
    print(n)


if __name__ == '__main__':
    main()
