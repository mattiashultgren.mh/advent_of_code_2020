

def update(world, limit, max_steps):
    w2 = []
    for ri, row in enumerate(world):
        r2 = []
        for ci, c in enumerate(row):
            if c == '.':
                r2.append('.')
            elif c == 'L':
                if neighbor_count_of(world, max_steps, ri, ci) == 0:
                    r2.append('#')
                else:
                    r2.append('L')
            else:
                if neighbor_count_of(world, max_steps, ri, ci) >= limit:
                    r2.append('L')
                else:
                    r2.append('#')
        w2.append(r2)
    return w2


def neighbor_count_of(world, max_steps, ri, ci):
    w, h = len(world[0]), len(world)
    count = 0
    for dr, dc in [(1, 1), (0, 1), (-1, 1), (1, 0), (-1, 0), (1, -1), (0, -1), (-1, -1)]:
        for d in range(1, max_steps + 1):
            rr, cc = ri + d*dr, ci + d*dc
            if (0 <= rr < h) and (0 <= cc < w):
                if world[rr][cc] == '#':
                    count += 1
                    break
                elif world[rr][cc] == 'L':
                    break
            else:
                break
    return count


def equal(w, w2):
    for r, r2 in zip(w, w2):
        for c, c2 in zip(r, r2):
            if c != c2:
                return False
    return True


def count_occupied(world):
    count = 0
    for r in world:
        count += sum(1 for x in r if x == '#')
    return count


def load_world():
    with open('day11.data') as file:
        world = []
        for line in file:
            line = line.replace('\n', '')
            world.append([x for x in line])
    return world


def solve_puzzle(limit, max_steps):
    world = load_world()
    while True:
        w2 = update(world, limit, max_steps)
        if equal(world, w2):
            break
        world = w2
    return count_occupied(world), world


def main():
    part1, world = solve_puzzle(4, 1)
    print(part1)
    part2, _ = solve_puzzle(5, max(len(world), len(world[0])))
    print(part2)


if __name__ == '__main__':
    main()
