
import collections


def main():
    memory = collections.defaultdict(int)
    mask_mask = 0
    mask_value = 0
    with open('day14.data') as file:
        for line in file:
            if line.startswith('mask'):
                mask_mask = int(line.replace('1', '0').replace('X', '1')[6:], 2)
                mask_value = int(line.replace('X', '0')[6:], 2)
            else:
                mem, _, value = line.split(' ')
                mem = int(mem[4:-1])
                value = int(value)
                memory[mem] = value & mask_mask | mask_value
    print(sum(memory.values()))

    memory = collections.defaultdict(int)
    mask_mask = 0
    mask_value = 0
    mask_floating = 0
    with open('day14.data') as file:
        for line in file:
            if line.startswith('mask'):
                mask_mask = int(line.replace('1', 'X').replace('0', '1').replace('X', '0')[6:], 2)
                mask_value = int(line.replace('X', '0')[6:], 2)
                mask_floating = int(line.replace('1', '0').replace('X', '1')[6:], 2)
            else:
                mem, _, value = line.split(' ')
                mem = int(mem[4:-1])
                value = int(value)
                for counter in range(2 ** bin(mask_floating).count('1')):
                    mask_counter = 0
                    tmp = mask_floating
                    while tmp:
                        bit = tmp & ~(tmp -1)
                        if counter & 1:
                            mask_counter |= bit
                        counter >>= 1
                        tmp ^= bit
                    memory[mem & mask_mask | mask_value | mask_counter] = value
    print(sum(memory.values()))


if __name__ == '__main__':
    main()
