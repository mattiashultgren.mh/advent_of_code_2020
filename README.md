# Advent of Code 2020

Just for fun puzzle solving

## Networkx
Day 7 was a fun problem with tree traversing.

## The slowest solve
Day 23 part 2, takes ~45 hours, ported it to C++ which runs in ~1 hour.
Surely there is a better way to solve this than my solution.

## Matching patterns
Day 19 is fully generic and didn't attempt to use tricks adapted to the special rules given.
Execution time at just under one second.

## Puzzles not fully solved by the code
Day 20, after a corner piece have been selected the orientation of that piece was hardcoded in manual for it to be the upper left corner.
