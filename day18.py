

def handle_parenthesis(expr, calc_func):
    while '(' in expr:
        idx = len(expr) - expr[::-1].index('(') - 1
        idx2 = idx + expr[idx:].index(')')

        expr[idx] = calc_func(expr[idx+1: idx2])
        del expr[idx+1:idx2+1]


def calculate(expr):
    handle_parenthesis(expr, calculate)

    result = expr[0]
    for idx in range(1, len(expr), 2):
        result = {'+': lambda a, b: a + b,
                  '*': lambda a, b: a * b
                  }[expr[idx]](result, expr[idx+1])
    return result


def calculate2(expr):
    handle_parenthesis(expr, calculate2)

    while '+' in expr:
        idx = expr.index('+')
        expr[idx-1] = expr[idx-1] + expr[idx+1]
        del expr[idx:idx+2]

    result = expr[0]
    for idx in range(1, len(expr), 2):
        result *= expr[idx+1]
    return result


def main():
    results, results2 = [], []
    with open('day18.data') as file:
        for line in file:
            line = line.replace('\n', '').replace(' ', '')
            expr = [int(x) if x.isnumeric() else x for x in line]
            results.append(calculate(expr.copy()))
            results2.append(calculate2(expr))
    print(sum(results))
    print(sum(results2))


if __name__ == '__main__':
    main()
