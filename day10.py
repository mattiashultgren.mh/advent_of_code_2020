
import collections


def count_arrangements(adapters, prev, target):
    if len(adapters) == 0:
        if target - prev <= 3:
            return 1
        return 0

    key = (adapters[0], prev)
    if key in count_arrangements.memo:
        return count_arrangements.memo[key]

    count = 0
    for skip in range(0, min(3, len(adapters))):
        if adapters[skip] - prev <= 3:
            count += count_arrangements(adapters[skip+1:], adapters[skip], target)

    count_arrangements.memo[key] = count
    return count
count_arrangements.memo = dict()


def main():
    adapters = sorted([int(x) for x in open('day10.data')])
    device = adapters[-1] + 3
    diffs = collections.defaultdict(int)
    prev = 0
    for n in adapters:
        diffs[n-prev] += 1
        prev = n
    diffs[3] += 1  # to the device
    print(diffs[1] * diffs[3])
    print(count_arrangements(adapters, 0, device))


if __name__ == '__main__':
    main()
