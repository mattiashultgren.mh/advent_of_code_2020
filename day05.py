

def main():
    seat_ids = set()
    with open('day05.data') as file:
        for line in file:
            row = int(line[:7].replace('F', '0').replace('B', '1'), 2)
            column = int(line[7:10].replace('L', '0').replace('R', '1'), 2)
            seat_id = row * 8 + column
            seat_ids.add(seat_id)
    max_seat_id = max(seat_id for seat_id in seat_ids)
    print(max_seat_id)
    for x in (set(range(max_seat_id)) - seat_ids):
        if (x-1) in seat_ids and (x+1) in seat_ids:
            print(x)


if __name__ == '__main__':
    main()
