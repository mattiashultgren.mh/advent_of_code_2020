

def main():
    card_key = 7573546
    door_key = 17786549
    mod = 20201227

    shared_key = 1
    subject = 7

    value = 1
    while value != door_key:
        value = (value * subject) % mod
        shared_key = (shared_key * card_key) % mod
    print(shared_key)


if __name__ == '__main__':
    main()
