

numbers = set()
target = 2020

with open('day01.data') as file:
    for line in file:
        n = int(line)
        left = target - n
        if left in numbers:
            print(n, left, n*left)
        numbers.add(n)

for n in numbers:
    for m in numbers:
        if n < m:
            left = target - n - m
            if left > m:
                if left in numbers:
                    print(n, m, left, n * m * left)
