

def is_sum_of_2(n, numbers):
    for idx, a in enumerate(numbers):
        for b in numbers[idx+1:]:
            if a+b == n:
                return True
    return False


def main():
    with open('day09.data') as file:
        numbers = [int(line) for line in file]

    for idx, n in enumerate(numbers[25:]):
        if not is_sum_of_2(n, numbers[idx:idx+25]):
            print(n)
            first_invalid = n
            break
    for idx in range(len(numbers)):
        for length in range(2, len(numbers)-idx):
            sum_range = sum(numbers[idx:idx+length])
            if sum_range == first_invalid:
                print(sum((min(numbers[idx:idx+length]), max(numbers[idx:idx+length]))))
            elif sum_range > first_invalid:
                break


if __name__ == '__main__':
    main()
