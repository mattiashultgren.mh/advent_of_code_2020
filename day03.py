

with open('day03.data') as file:
    world = [line.replace('\n', '') for line in file]

width = len(world[0])

product = 1
for right, down in [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]:
    trees = 0
    x, y = 0, 0
    while y < len(world):
        if world[y][x % width] == '#':
            trees += 1
        x += right
        y += down

    product *= trees
    print(right, down, trees, product)
