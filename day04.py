

def validate(passport):
    for key in ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']:
        if key not in passport:
            return False
    return True


def validate_strict(passport):
    if not validate(passport):
        return False

    if not (1920 <= int(passport['byr']) <= 2002):
        return False

    if not (2010 <= int(passport['iyr']) <= 2020):
        return False

    if not (2020 <= int(passport['eyr']) <= 2030):
        return False

    if passport['hgt'][-2:] == 'cm':
        if not (150 <= int(passport['hgt'][:-2]) <= 193):
            return False
    elif passport['hgt'][-2:] == 'in':
        if not (59 <= int(passport['hgt'][:-2]) <= 76):
            return False
    else:
        return False

    if len(passport['hcl']) != 7:
        return False
    if passport['hcl'][0] != '#':
        return False
    for c in passport['hcl'][1:]:
        if c not in '1234567890abcdef':
            return False

    if passport['ecl'] not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
        return False

    if len(passport['pid']) != 9:
        return False
    return True


def main():
    passports = []
    with open('day04.data') as file:
        passport = {}
        for line in file:
            line = line.replace('\n', '')
            if len(line) > 0:
                for fields in line.split(' '):
                    key, value = fields.split(':')
                    passport[key] = value
            else:
                passports.append(passport)
                passport = {}
        if len(passport) > 0:
            passports.append(passport)

    print(sum(1 for p in passports if validate(p)))
    print(sum(1 for p in passports if validate_strict(p)))


if __name__ == '__main__':
    main()
