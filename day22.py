

def load_decks(filename='day22.data'):
    p1, p2 = [], []
    with open(filename) as file:
        active_deck = p1
        for line in file:
            if 'Player' in line:
                if '2' in line:
                    active_deck = p2
            elif line[0].isnumeric():
                active_deck.append(int(line))
    return p1, p2


def play_game(p1, p2):
    while min(len(p1), len(p2)) > 0:
        n1, n2 = p1.pop(0), p2.pop(0)
        if n1 > n2:
            p1.extend([n1, n2])
        else:
            p2.extend([n2, n1])


def get_hashable_rep(p1, p2):
    return ','.join(str(x) for x in p1) + '|' + ','.join(str(x) for x in p2)


def play_recursive_game(p1, p2):
    configs = set()
    while min(len(p1), len(p2)) > 0:
        rep = get_hashable_rep(p1, p2)
        if rep in configs:
            return 1
        configs.add(rep)
        n1, n2 = p1.pop(0), p2.pop(0)
        if len(p1) >= n1 and len(p2) >= n2:
            if play_recursive_game(p1[:n1], p2[:n2]) == 1:
                p1.extend([n1, n2])
            else:
                p2.extend([n2, n1])
        elif n1 > n2:
            p1.extend([n1, n2])
        else:
            p2.extend([n2, n1])

    return 1 if len(p1) > len(p2) else 2


def main():
    p1, p2 = load_decks()
    play_game(p1, p2)
    if len(p1) > len(p2):
        winning_deck = p1
    else:
        winning_deck = p2
    print(sum((idx+1) * n for idx, n in enumerate(reversed(winning_deck))))

    p1, p2 = load_decks('day22.data')
    if play_recursive_game(p1, p2) == 1:
        winning_deck = p1
    else:
        winning_deck = p2
    print(sum((idx+1) * n for idx, n in enumerate(reversed(winning_deck))))


if __name__ == '__main__':
    main()
