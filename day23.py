


def make_move(current, current_idx, cups, max_value):
    base = current_idx
    if base + 4 < len(cups):
        pickup = cups[base + 1: base + 4]
        del cups[base + 1: base + 4]
    else:
        pickup = []
        for _ in range(3):
            idx = 0 if (base + 1) >= len(cups) else (base + 1)
            pickup.append(cups[idx])
            del cups[idx]
            if idx == 0:
                current_idx -= 1

    dest = (current - 2) % max_value + 1
    while dest in pickup:
        dest = (dest - 2) % max_value + 1
    dest_idx = cups.index(dest) + 1
    cups[dest_idx:dest_idx] = pickup

    if dest_idx <= current_idx:
        current_idx += 3
    current_idx = (current_idx + 1) % len(cups)
    return cups[current_idx], current_idx


def main():
    cups = [int(x) for x in '523764819']
    current, current_idx = cups[0], 0
    for move in range(100):
        current, current_idx = make_move(current, current_idx, cups, 9)

    result = [str(cups[(cups.index(1) + 1 + idx) % len(cups)]) for idx in range(8)]
    print(''.join(result))

    cups = [int(x) for x in '523764819']
    max_value = 1_000_000
    cups.extend([x for x in range(10, max_value + 1)])

    current, current_idx = cups[0], 0
    for move in range(10_000_000):
        if move % 1000 == 999:
            print(move + 1)
        current, current_idx = make_move(current, current_idx, cups, max_value)
    idx = cups.index(1)
    a = cups[(idx + 1) % len(cups)]
    b = cups[(idx + 2) % len(cups)]
    print(a * b)


if __name__ == '__main__':
    main()


# max_value -> time (answer)
# 10^1 -> 13s (12)
# 10^2 -> 17s (5103)
# 10^3 -> 76s (735111)
# 10^4 -> 716s (20576304)
# 10^5 -> 9321s (237136185)
# 10^6 -> 161066s (511780369955) (real puzzle)
