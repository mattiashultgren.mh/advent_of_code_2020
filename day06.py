

def get_person():
    with open('day06.data') as file:
        for line in file:
            yield set(line.replace('\n', ''))


def find_total_sum(update_func):
    total_sum = 0
    questions = None
    for p in get_person():
        if len(p) == 0:
            total_sum += len(questions)
            questions = None
        elif questions is None:
            questions = p
        else:
            questions = update_func(questions, p)
    return total_sum + len(questions)


if __name__ == '__main__':
    print(find_total_sum(lambda q, p: q | p))
    print(find_total_sum(lambda q, p: q & p))
