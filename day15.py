

def main():
    data_idx = {}
    for last_index, last_number in enumerate([12, 20, 0, 6, 1, 17]):
        data_idx[last_number] = last_index

    last_number = 7
    idx = 5
    for limit in [2020, 30_000_000]:
        for idx in range(idx + 1, limit-1):
            number = idx - data_idx.get(last_number, idx)
            data_idx[last_number] = idx
            last_number = number
        print(last_number)


if __name__ == '__main__':
    main()
