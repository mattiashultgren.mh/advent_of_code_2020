

def load_data():
    images = {}
    with open('day20.data') as file:
        for line in file:
            _, img_id = line.split(' ')
            img_id = int(img_id[:-2])
            img_data = [file.readline().replace('\n', '') for _ in range(10)]
            images[img_id] = img_data
            file.readline()
    return images


def get_edges_normal(img):
    yield img[0]
    yield img[-1]
    yield ''.join([line[0] for line in img])
    yield ''.join([line[-1] for line in img])


def get_edges(img):
    for edge in get_edges_normal(img):
        yield edge
        yield ''.join(reversed(edge))


def rotate(img):
    rot = []
    for idx in range(len(img)):
        line = ''.join(reversed([line[idx] for line in img]))
        rot.append(line)
    return rot


def flip(img):
    return [x for x in reversed(img)]


def get_orientations(img):
    for _ in range(4):
        yield img
        yield flip(img)
        img = rotate(img)


def get_right_edge(img):
    return ''.join([line[-1] for line in img])


def get_left_edge(img):
    return ''.join([line[0] for line in img])


def get_top_edge(img):
    return img[0]


def get_bottom_edge(img):
    return img[-1]


def show(img):
    print()
    for line in img:
        print(line)


def replace(image, monster, x, y):
    for yy in range(len(monster)):
        line = []
        for xx, c in enumerate(image[y+yy]):
            if 0 <= (xx - x) < len(monster[0]):
                if monster[yy][xx-x] == '#':
                    c = 'O'
            line.append(c)
        image[y+yy] = ''.join(line)


def match_and_replace(image, monster, x, y):
    for xx in range(len(monster[0])):
        for yy in range(len(monster)):
            if monster[yy][xx] == '#':
                if image[y+yy][x+xx] != '#':
                    return
    replace(image, monster, x, y)


def main():
    images = load_data()

    magic_number = 1
    for img_id, img in images.items():
        print(img_id)
        matches = 0
        for img_id2, img2 in images.items():
            if img_id == img_id2:
                continue
            match = False
            for edge in get_edges(img):
                for edge2 in get_edges(img2):
                    if edge == edge2:
                        match = True
            if match:
                matches += 1
                print('  ', img_id2)
        if matches == 2:
            corner_id = img_id
            magic_number *= img_id
    print()
    print(magic_number)

    patch_size = len(images[corner_id])
    full_image = []
    while len(images) > 0:
        if len(full_image) == 0:
            patch_row = rotate(rotate(images[corner_id]))
            del images[corner_id]
        else:
            pattern_to_match = get_bottom_edge(full_image)[:patch_size]
            matched = False
            for img_id, img in images.items():
                for img_mod in get_orientations(img):
                    pattern = get_top_edge(img_mod)
                    if pattern_to_match == pattern:
                        matched = True
                        del images[img_id]
                        break
                if matched:
                    break
            patch_row = img_mod

        while True:
            pattern_to_match = get_right_edge(patch_row)
            matched = False
            for img_id, img in images.items():
                for img_mod in get_orientations(img):
                    pattern = get_left_edge(img_mod)
                    if pattern_to_match == pattern:
                        matched = True
                        del images[img_id]
                        break
                if matched:
                    break
            if not matched:
                break
            patch_row = [x+y for x, y in zip(patch_row, img_mod)]

        full_image.extend(patch_row)
    #show(full_image)

    image = []
    for idx, line in enumerate(full_image):
        if idx % patch_size not in [0, patch_size - 1]:
            line_mod = ''.join([x for idx2, x in enumerate(line) if idx2 % patch_size not in [0, patch_size - 1]])
            image.append(line_mod)
    #show(image)

    monster = ['                  # ',
               '#    ##    ##    ###',
               ' #  #  #  #  #  #   ']
    for idx in range(8):
        if idx & 1 == 1:
            image = rotate(image)
        else:
            image = flip(image)

        for x in range(len(image[0]) - len(monster[0])):
            for y in range(len(image) - len(monster)):
                match_and_replace(image, monster, x, y)
    #show(image)

    print(''.join(image).count('#'))

if __name__ == '__main__':
    main()
