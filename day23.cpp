
#include <vector>
#include <algorithm>
#include <iterator>
#include <iostream>


int pos_mod(int a, int b)
{
    return (b + a % b) % b;
}


void make_move(int &current, int &current_idx, std::vector<int> &cups, int max_value)
{
    int base = current_idx;
    int pickup[3];

    if(base + 4 < cups.size())
    {
        const auto begin = std::next(cups.begin(), base + 1);
        const auto end = std::next(cups.begin(), base + 4);
        std::copy(begin, end, pickup);
        cups.erase(begin, end);
    }
    else
    {
        for(int i=0; i<3; i++)
        {
            auto iter = cups.begin();
            if((base + 1) < cups.size())
            {
                iter = std::next(cups.begin(), base + 1);
            }
            pickup[i] = *iter;
            cups.erase(iter);
            if(iter == cups.begin())
            {
                current_idx -= 1;
            }
        }
    }

    int dest = current;
    do{
        dest = pos_mod(dest - 2, max_value) + 1;
    }while((dest == pickup[0]) || (dest == pickup[1]) || (dest == pickup[2]));
    auto dest_iter = std::next(std::find(cups.begin(), cups.end(), dest), 1);
    cups.insert(dest_iter, pickup, pickup + 3);

    if((dest_iter - cups.begin()) <= current_idx)
    {
        current_idx += 3;
    }
    current_idx = (current_idx + 1) % cups.size();
    current = cups[current_idx];
}


void part1(void)
{
    std::vector<int> cups = {5, 2, 3, 7, 6, 4, 8, 1, 9};
    int current = cups[0];
    int current_idx = 0;

    for(int move=0; move<100; move++)
    {
        make_move(current, current_idx, cups, 9);
    }

    int result = 0;
    for(int idx=0; idx<8; idx++)
    {
        result = result * 10 + cups[((std::find(cups.begin(), cups.end(), 1) - cups.begin()) + 1 + idx) % cups.size()];
    }
    std::cout << result << std::endl;
}


void part2(void)
{
    std::vector<int> cups = {5, 2, 3, 7, 6, 4, 8, 1, 9};
    int current = cups[0];
    int current_idx = 0;

    int max_value = 1000*1000;
    for(int val=10; val<=max_value; val++)
    {
        cups.push_back(val);
    }

    for(int move=0; move<10*1000*1000; move++)
    {
        make_move(current, current_idx, cups, max_value);
    }
    int idx = std::find(cups.begin(), cups.end(), 1) - cups.begin();
    long a = cups[(idx + 1) % cups.size()];
    long b = cups[(idx + 2) % cups.size()];
    std::cout << (a * b) << std::endl;
}


int main(void)
{
    part1();
    part2();
}


// max_value -> time (answer)
// 10^1 -> 1s (12)
// 10^2 -> 1s (5103)
// 10^3 -> 3s (735111)
// 10^4 -> 32s (20576304)
// 10^5 -> 366s (237136185)
// 10^6 -> 3572s (511780369955) (real puzzle)
