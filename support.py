
import time


def timing(func):
    def wrapper(*args, **kwargs):
        start = time.time()
        r = func(*args, **kwargs)
        print(time.time() - start)
        return r
    return wrapper


def show_world(world):
    for r in world:
        print(''.join(r))
