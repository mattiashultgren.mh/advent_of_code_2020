

def ship_moving(dx, dy, move_ship_on_nwse):
    x, y = 0, 0
    with open('day12.data') as file:
        for line in file:
            action, value = line[0], int(line[1:])
            if action == 'R':
                for _ in range(0, value, 90):
                    dx, dy = dy, -dx
            elif action == 'L':
                for _ in range(0, value, 90):
                    dx, dy = -dy, dx
            elif action == 'F':
                x, y = x + value*dx, y + value*dy
            elif move_ship_on_nwse:
                if action == 'N':
                    y += value
                elif action == 'S':
                    y -= value
                elif action == 'E':
                    x += value
                elif action == 'W':
                    x -= value
            else:
                if action == 'N':
                    dy += value
                elif action == 'S':
                    dy -= value
                elif action == 'E':
                    dx += value
                elif action == 'W':
                    dx -= value
    return x, y


def main():
    x, y = ship_moving(1, 0, True)
    print(abs(x)+abs(y))

    x, y = ship_moving(10, 1, False)
    print(abs(x)+abs(y))


if __name__ == '__main__':
    main()
