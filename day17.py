
import itertools


def load_world():
    world = set()
    with open('day17.data') as file:
        for y, line in enumerate(file):
            for x, point in enumerate(line):
                if point == '#':
                    world.add((x, y, 0, 0))
    return {0, x}, {0, y}, {0}, {0}, world


def get_neighbors(x, y, z, w, world):
    count = 0
    for dx, dy, dz, dw in itertools.product(range(-1, 2), range(-1, 2), range(-1, 2), range(-1, 2)):
        if not (dx == 0 and dy == 0 and dz == 0 and dw == 0):
            if (x+dx, y+dy, z+dz, w+dw) in world:
                count += 1
    return count


def add(xlim, ylim, zlim, wlim, world, x, y, z, w):
    world.add((x, y, z, w))
    xlim.add(x)
    ylim.add(y)
    zlim.add(z)
    wlim.add(w)


def update(xlim, ylim, zlim, wlim, world, only_3d=False):
    world2 = set()
    for x, y, z, w in itertools.product(range(min(xlim) - 1, max(xlim) + 2),
                                        range(min(ylim) - 1, max(ylim) + 2),
                                        range(min(zlim) - 1, max(zlim) + 2),
                                        [0] if only_3d else range(min(wlim) - 1, max(wlim) + 2)):
        neighbors = get_neighbors(x, y, z, w, world)
        allowed = [3]
        if (x, y, z, w) in world:
            allowed.append(2)
        if neighbors in allowed:
            add(xlim, ylim, zlim, wlim, world2, x, y, z, w)
    return world2


def run_process(only_3d):
    xlim, ylim, zlim, wlim, world = load_world()
    for iteration in range(6):
        world = update(xlim, ylim, zlim, wlim, world, only_3d=only_3d)
    print(len(world))


def main():
    run_process(True)
    run_process(False)


if __name__ == '__main__':
    main()
